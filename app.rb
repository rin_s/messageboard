﻿# coding: utf-8
Encoding.default_external = 'utf-8'

require 'sinatra'
require 'sinatra/reloader'
require 'mongo'

before do
  db = Mongo::Client.new(['127.0.0.1:27017'], :database => 'sinatra_test')
  @comments = db[:comments]
end

helpers do
  include Rack::Utils
  alias_method :h, :escape_html
end 

get '/' do
  erb :index
end

post '/comment' do
  posttime = Time.now.strftime('%Y/%m/%d %H:%M:%S')
  @comments.insert_one({:name => params[:name], :comment => params[:comment], :posttime => "#{posttime}"})
  redirect '/'
end